require 'singleton'

class PhonesMapping < Hash
  include Singleton

  def initialize
    merge! YAML.load_file 'config/phones_mapping.yml'
    logger.info "Pre-configured phone numbers mapping: #{self}"
  end

  def self.set(hash)
    logger.info "Setting phones mapping: #{hash}"
    hash.each { |k, v| instance[k.to_i] = v }
    logger.info "Current mapping: #{instance}"
  end

  def self.remove(array)
    logger.info "Removing mapping for numbers: #{array}"
    instance.reject! { |k, _| array.include? k }
    logger.info "Current mapping: #{instance}"
  end

  def self.match(transit_number)
    instance[instance.keys.find { |k| transit_number =~ /#{k}/ }]
  end

  def self.partner_id_by_real_number(real_number)
    Hash[*(instance.values.map { |e| e.reverse }.flatten)][real_number.to_s]
  end
end
