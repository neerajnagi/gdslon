require 'json'
require 'singleton'

class Messenger
  include Singleton

  attr_reader :redis

  def initialize
    @redis = get_redis
=begin
    Thread.new do
      get_redis.subscribe :slonofon_set, :slonofon_unset do |on| #Using another redis connection for sub
        on.message do |channel, message|
          begin
            msg = JSON.parse message
            channel.to_sym == :slonofon_set ? PhonesMapping.set(msg) : PhonesMapping.remove(msg)
          rescue Exception => e
            logger.error e.message
          end
        end
      end
    end

    @redis.publish :slonofon_up, "Up at #{Time.now}"
=end 
 end

  def self.send_call_details(details)
    logger.info "updating cdrs at gdslon: #{details}"
  

end

  def get_redis
    @redis_config ||= if File.exists?(file = 'config/redis.yml')
                        YAML.load_file(file)
                      else
                        logger.warn "Could not find #{file} , using defaults for Redis connection"
                        {}
                      end
    logger.info "New Redis connection, configuration in use: #{@redis_config}"
    Redis.new @redis_config
  end
end
