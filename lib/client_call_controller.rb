require "#{File.dirname(__FILE__)}/phones_mapping"
require "#{File.dirname(__FILE__)}/shop_confirmation_controller"

class ClientCallController < Adhearsion::CallController
  before_call do
    call[:start] = Time.now #Странно, почему-то сами они это не хранят
  end
  def run
    partner, shop = PhonesMapping.match call.to
    logger.info "caller id: #{call.from}, partner id: #{partner}, shop id: #{shop}"
	PeriodicSync.init_cdr(call.id)
    call[:affiliate_id]=partner
     call[:redirected_number]= shop
    
    answer
    play '/home/adhearsion/slonophone/play.wav', renderer: :freeswitch
    moh = play! '/home/adhearsion/slonophone/moh.wav', renderer: :freeswitch , repeat:100
#	        call.on_joined {moh.stop!}
    logger.info "dialing shop: #{shop}"
	call[:parent_call_id]=call.id
	call[:dial_result] = "not_dialed"
    dial_result = dial "{origination_caller_id_number=4997037001}sofia/external/#{shop}@213.170.100.150", confirm: ShopConfirmationController, confirm_metadata:call
  call[:dial_result] = dial_result.result.to_s
 logger.info "call to shop #{shop} ended"
  hangup
  end
 after_call do
	call[:end] = Time.now
    PeriodicSync.push_cdr(  {
				affiliate_id: call[:affiliate_id],
				redirected_number: call[:redirected_number],
				duration: call[:end].to_i - call[:start].to_i  ,
				duration_redirected: (call[:start_redirect].nil?)? 0: call[:end].to_i - call[:start_redirect].to_i, 
				started_at: call[:start].in_time_zone(CDR_TIMEZONE).strftime("%Y-%m-%d %H:%M:%S"), # .strftime('%d/%b/%Y:%H:%I:%S'),
                                ended_at: call[:end].in_time_zone(CDR_TIMEZONE).strftime("%Y-%m-%d %H:%M:%S") , 
			#	filename: call[:filename],
                                number: call.to.split('@').first,
                                uuid: call.id
			#	status: call[:dial_result]
				
			})
  end
end
