require 'timers'
require 'java'
require 'jdbc/postgres'
require 'celluloid'
require 'active_support/time'
Jdbc::Postgres.load_driver
class PeriodicSync
  include Celluloid

=begin
  SYNC_INTERVAL=60
  DB_HOST =  "app3.gdeslon.ru"
  DB_USERNAME= "slonophone"
  DB= "gdeslon"
  DB_PASSWORD = "rk7Wo0SWDrk7Su"
=end
  def run
    timer = Timers.new
    begin
      Java::org.postgresql.Driver
      userurl = "jdbc:postgresql://#{DB_HOST}/#{DB}"
      connSelect = java.sql.DriverManager.get_connection(userurl, DB_USERNAME, DB_PASSWORD)
      stmtSelect = connSelect.create_statement
      selectQuery = <<-SQL
         SELECT phc.partner_id, phn.number, u.phone
         FROM "phone_cooperations" as phc
          INNER JOIN "phone_numbers" as phn
            ON phc.phone_number_id = phn.id
          INNER JOIN "users" as u
            ON phc.advertiser_id= u.id  
      SQL
      logger.info "<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>   Running scheduled sync"
      rsS = stmtSelect.execute_query(selectQuery)
      while (rsS.next) do

	logger.info "#{rsS.getObject("number")} => #{rsS.getObject("partner_id")}, #{rsS.getObject("phone")}"
        PhonesMapping.set( {rsS.getObject("number") => [rsS.getObject("partner_id"), rsS.getObject("phone") ] })
      end
      timer.every(SYNC_INTERVAL) do 
        logger.info "<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>   Running scheduled sync"
        rsS = stmtSelect.execute_query(selectQuery)
        while (rsS.next) do
          PhonesMapping.set( {rsS.getObject("number") =>[rsS.getObject("partner_id"), rsS.getObject("phone") ] })	
        end
      end
    end

    loop {timer.wait }
  end


  def self.init_cdr(uuid)
    logger.info "<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>    Pushing CDR with data #{uuid}"
    Java::org.postgresql.Driver
    userurl = "jdbc:postgresql://#{DB_HOST}/#{DB}"
    connInsert = java.sql.DriverManager.get_connection(userurl, DB_USERNAME, DB_PASSWORD)
    stmtInsert = connInsert.create_statement
    insertQuery = <<-SQL
    INSERT INTO phone_entries (uuid,filename)
    VALUES ('#{uuid}' ,'Not Applicable, Redirected call not answered' )
    SQL
    # <<-SQL
    # INSERT INTO phone_entries (affiliate_id, number, redirected_number, filename, uuid, started_at, ended_at,created_at, updated_at , duration, duration_redirected, status)
    # VALUES (#{cdr[:affiliate_id].to_i},'#{cdr[:number]}','#{ cdr[:redirected_number]}', '#{cdr[:filename]}', '#{cdr[:uuid]}', STR_TO_DATE('#{cdr[:started_at]}','%d/%b/%Y:%H:%I:%S' ), STR_TO_DATE('#{cdr[:ended_at]}', '%d/%b/%Y:%H:%I:%S'),STR_TO_DATE('#{Time.now.utc.strftime('%d/%b/%Y:%H:%I:%S')}', '%d/%b/%Y:%H:%I:%S')  ,STR_TO_DATE('#{Time.now.utc.strftime('%d/%b/%Y:%H:%I:%S')}', '%d/%b/%Y:%H:%I:%S')  , #{cdr[:duration].to_i}, #{cdr[:duration_redirected].to_i},'#{cdr[:status]}' )
    # SQL
    stmtInsert.execute_update(insertQuery)
    stmtInsert.close
    connInsert.close
  end

  def self.push_cdr(cdr)
    logger.info "<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>    Pushing CDR with data #{cdr}"
    Java::org.postgresql.Driver
    userurl = "jdbc:postgresql://#{DB_HOST}/#{DB}"
    connInsert = java.sql.DriverManager.get_connection(userurl, DB_USERNAME, DB_PASSWORD)
    stmtInsert = connInsert.create_statement
    insertQuery = <<-SQL
      UPDATE phone_entries 
      SET  
      partner_id=#{cdr[:affiliate_id].to_i}, 
      number='#{cdr[:number]}', 
      number_redirected='#{ cdr[:redirected_number]}',  
      started_at='#{cdr[:started_at]}', 
      ended_at='#{cdr[:ended_at]}', 
      created_at='#{Time.now.in_time_zone(CDR_TIMEZONE).strftime("%Y-%m-%d %H:%M:%S")}', 
      updated_at='#{Time.now.in_time_zone(CDR_TIMEZONE).strftime("%Y-%m-%d %H:%M:%S")}', 
      duration=#{cdr[:duration].to_i}, 
      duration_redirected=#{cdr[:duration_redirected].to_i} 
      WHERE uuid='#{cdr[:uuid] }'
    SQL

    # "INSERT INTO phone_entries (affiliate_id, number, redirected_number, filename, uuid, started_at, ended_at,created_at, updated_at , duration, duration_redirected, status) VALUES (#{cdr[:affiliate_id].to_i},'#{cdr[:number]}','#{ cdr[:redirected_number]}', '#{cdr[:filename]}', '#{cdr[:uuid]}', STR_TO_DATE('#{cdr[:started_at]}','%d/%b/%Y:%H:%I:%S' ), STR_TO_DATE('#{cdr[:ended_at]}', '%d/%b/%Y:%H:%I:%S'),STR_TO_DATE('#{Time.now.utc.strftime('%d/%b/%Y:%H:%I:%S')}', '%d/%b/%Y:%H:%I:%S')  ,STR_TO_DATE('#{Time.now.utc.strftime('%d/%b/%Y:%H:%I:%S')}', '%d/%b/%Y:%H:%I:%S')  , #{cdr[:duration].to_i}, #{cdr[:duration_redirected].to_i},'#{cdr[:status]}' )"

    # "INSERT INTO phone_entries (partner_id, number, number_redirected, filename, uuid, started_at, ended_at,created_at, updated_at , duration, duration_redirected, status) VALUES (#{cdr[:affiliate_id].to_i},'#{cdr[:number]}','#{ cdr[:redirected_number]}', '#{cdr[:filename]}', '#{cdr[:uuid]}', STR_TO_DATE('#{cdr[:started_at]}','%d/%b/%Y:%H:%I:%S' ), STR_TO_DATE('#{cdr[:ended_at]}', '%d/%b/%Y:%H:%I:%S'),STR_TO_DATE('#{Time.now.utc.strftime('%d/%b/%Y:%H:%I:%S')}', '%d/%b/%Y:%H:%I:%S')  ,STR_TO_DATE('#{Time.now.utc.strftime('%d/%b/%Y:%H:%I:%S')}', '%d/%b/%Y:%H:%I:%S')  , #{cdr[:duration].to_i}, #{cdr[:duration_redirected].to_i},'#{cdr[:status]}' )"
    stmtInsert.execute_update(insertQuery)
    stmtInsert.close
    connInsert.close
  end

  def self.update_cdr(column, data,where_key, where_val)
    logger.info "<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>    Updating CDR with ->>  UPDATE phone_entries SET #{column}='#{data}' WHERE #{where_key}='#{where_val}'   "
    Java::org.postgresql.Driver
    userurl = "jdbc:postgresql://#{DB_HOST}/#{DB}"
    connInsert = java.sql.DriverManager.get_connection(userurl, DB_USERNAME, DB_PASSWORD)
    stmtInsert = connInsert.create_statement
    insertQuery = <<-SQL
      UPDATE phone_entries SET #{column}='#{data}' WHERE #{where_key}='#{where_val}'
    SQL
    stmtInsert.execute_update(insertQuery)
    stmtInsert.close
    connInsert.close
  end

end

#PeriodicSync.update_cdr('filename', 'xxxxxxxxxxxxxxx', 'uuid', '5')
