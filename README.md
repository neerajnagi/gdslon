# Slonofon
### Adhearsion based voice application for GdeSlon affiliate marketing network

###Installation & Configuration
```
\curl -L https://get.rvm.io | bash 
source /etc/profile.d/rvm.sh
rvm install jruby
rvm --default use jruby
cd /home/adhearsion/slonophone
rvm use jruby
edit Gemfile with
	gem adherasion , '~> 2.4'
bundle update adhearsion

```

###Monitoring
##slonophone 
```
/etc/god/ahn.god contains god script for monitoring slonophone adhearsion app
/etc/god/freeswitch.god contains god script for monitoring freeswitch

```

###Start/Stop
```
to start/stop/restart adhearsion component
	god start ahn
	god stop ahn

to start/stop/restart freeswitch component
	god start freeswitch
	god stop freeswitch

to start/stop/restart both component
	god start slonophone
	god stop slonophone

```

###DB Sync
```
perfomed by lib/periodic_sync.rb
configurable params present in config/environment.rb, most important being SYNC_INTERVAL(seconds), default value 60 second
```
