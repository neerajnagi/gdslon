require 'slon-service'
require 'pg'
require 'redis'

APP_ROOT = Dir.pwd
POSTGRES = SlonService::PostgresConnector.new
REDIS = SlonService::RedisConnector.new
KEY_EXPIRE = 180 * 24 * 3600 # half year
DEFAULT_AFFILIATE = 2411

use Rack::Reloader, 0 if SlonService::App.development?
#use Rack::CommonLogger, SlonService::Logger.new(File.join(APP_ROOT,'log.log')) if SlonService::App.production?
use Rack::ContentLength

class Service < SlonService::Service

  def get_phone
    token = params[:gs_token]

    return render_404 if token.blank?

    transition_key = "transition:#{token}"
    transition = REDIS.get transition_key

    if transition.blank?
      transition = POSTGRES.query <<-SQL
        SELECT affiliate_id, merchant_id FROM transitions WHERE token = #{POSTGRES.escape(token)}
      SQL
      transition = transition.to_a[0]

      return render_404 unless transition

      phone_id = POSTGRES.query <<-SQL
        SELECT phone_number_id FROM phone_cooperations WHERE advertiser_id = #{transition['merchant_id']} AND partner_id = #{transition['affiliate_id']}
      SQL
      phone_id = phone_id.to_a[0]
      phone_id = phone_id['phone_number_id'] if phone_id

      unless phone_id
        phone_id = MYSQL.query <<-SQL
        SELECT phone_number_id FROM phone_cooperations WHERE advertiser_id = #{transition['merchant_id']} AND partner_id = #{DEFAULT_AFFILIATE}
        SQL
        phone_id = phone_id.to_a[0]
        phone_id = phone_id['phone_number_id'] if phone_id
      end

      if phone_id
        phone = POSTGRES.query <<-SQL
          SELECT number FROM phone_numbers WHERE id = #{phone_id}
        SQL
        phone = phone.to_a[0]

        transition.merge!(phone) if phone
      end
      transition = transition.to_json

      REDIS.set  transition_key, transition
      REDIS.expire transition_key, KEY_EXPIRE
    end

    response transition
  end

  private

  def routing(path)
    case path
      when '/get_phone' then get_phone
      else
        render_404
    end
  end
end

run Service.new
