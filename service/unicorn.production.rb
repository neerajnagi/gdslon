USER  = 'deployer'
GROUP = 'deployer'
SERVICE = 'slon-getphone'
PORT  = 5000


# TODO: Move the following code to slon-service gem

worker_processes 4
working_directory Dir.pwd
listen "/tmp/#{SERVICE}.sock", :backlog => 64
listen PORT, :tcp_nopush => true
timeout 30
pid "/var/run/#{SERVICE}.pid"
#pid File.join(Dir.pwd, "#{SERVICE}.pid")
#stderr_path File.join(Dir.pwd, 'stderr.log')
#stdout_path File.join(Dir.pwd, 'stdout.log')

require 'syslogger'
logger Syslogger.new("#{SERVICE}", Syslog::LOG_PID, Syslog::LOG_LOCAL0)

preload_app true
check_client_connection false

before_fork do |server, worker|
  # Graceful reload
  old_pid = "/var/run/#{SERVICE}.pid.oldbin"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill('QUIT', File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end
end

after_fork do |server, worker|
  # Force to use specified USER and GROUP
  begin
    uid, gid = Process.euid, Process.egid
    user, group = USER, GROUP
    target_uid = Etc.getpwnam(user).uid
    target_gid = Etc.getgrnam(group).gid
    worker.tmp.chown(target_uid, target_gid)
    if uid != target_uid || gid != target_gid
      Process.initgroups(user, target_gid)
      Process::GID.change_privilege(target_gid)
      Process::UID.change_privilege(target_uid)
    end
  rescue => e
    raise e unless ENV['RACK_ENV'] == 'development'
  end
end
